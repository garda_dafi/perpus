@extends('perpus')

@section('content')
  <div class="row">
      <div class="col-6 offset-3">
          <div class="card">
              <div class="card-header text-center">{{ __('Reset Password') }}</div>

              <div class="card-body">
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif

                  <form method="POST" action="{{ route('password.email') }}">
                      @csrf

                      <div class="form-group">
                          <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Alamat E-Mail') }}</label>

                          <div class="col-md-6 offset-4">
                              <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                              @if ($errors->has('email'))
                                  <span class="invalid-feedback">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  {{ __('Kirim Password Reset Link') }}
                              </button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
</div>
@endsection

@extends('perpus')
@section('content')
  <div class="row">
    <div class="col-6 offset-3">
      @if(session()->has('success-messages'))
      <div class="alert alert-success">
        {{ session()->get('success-messages') }}
      </div>
      @endif
      <table class="table" style="text-transform:uppercase">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>Peminjaman</th>
                <th class="text-right">Actions</th>
            </tr>
        </thead>
        <tbody>
          @foreach ($borrows as $key=>$borrow)
            <tr>
              <td class="text-center">{{ ++$key }}</td>
              <td>{{ $borrow->buku->judul }}</td>
              <td class="td-actions text-right">
                <a href="{{ action('PeminjamanController@delete', $borrow->id) }}" class="btn btn-primary">Kembalikan</a>
              </td>
            </tr>
          @endforeach
        </tbody>
    </table>
    </div>
  </div>

@endsection

@extends('perpus')

@section('content')
<div class="row">
  <div class="col-4 offset-4">
    <div class="card">
      <div class="card-body">
        <form method="POST" action="{{ action('UserController@update', Auth::user()->id) }}" enctype="multipart/form-data" >
          <div class="form-group">
            <label for="username" class="bmd-label-floating">User Name</label>
            <input type="text" class="form-control" id="username" name="username" value="{{ Auth::user()->username }}">
          </div>
          <div class="form-group">
            <label for="name" class="bmd-label-floating">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ Auth::user()->name }}">
          </div>
          <div class="form-group">
            <label for="email" class="bmd-label-floating">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}">
          </div>
          <div class="form-group">
            <label for="alamat" class="bmd-label-floating">Alamat</label>
            <textarea name="alamat" class="form-control">{{ Auth::user()->alamat }}</textarea>
          </div>
          <div class="form-group">
            <label for="password" class="bmd-label-floating">Password</label>
            <input type="password" class="form-control" id="password" name="password">
          </div>
          <div class="form-group">
            <label for="re_password" class="bmd-label-floating">Ulangi Password</label>
            <input type="password" class="form-control" id="re_password" name="re_password">
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-primary">Ubah</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

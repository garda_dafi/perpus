@extends('perpus')
@section('content')
<div class="row">
  <div class="col-md-6 offset-3">
    <div class="card">
      <div class="card-body">
        <img class="card-img-top" style="width:30%;float:left;margin-right:30px" src="{{ asset('/images/'.$buku->url_cover) }}" alt="Card image cap">
        <h3 class="card-title">{{ $buku->judul }}</h3>
        <p class="card-text">{{ $buku->sinopsis }}</p>
      </div>
      <div class="card-footer">
        <a href="{{ action('PeminjamanController@create', $buku->id) }}" class="btn btn-primary">Pinjam</a>
      </div>
    </div>
  </div>
</div>
@endsection

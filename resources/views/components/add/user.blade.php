@extends('perpus')

@section('content')
<div class="row">
  <div class="col-4 offset-4">
    <div class="card">
      <div class="card-body">
        <form method="POST" action="{{ action('UserController@store') }}" enctype="multipart/form-data" >
          <div class="form-group @if($errors->has('username')) @endif">
            <label for="username" class="bmd-label-floating">User Name</label>
            <input type="text" class="form-control" id="username" name="username">
            <span class="text-danger">{{ $errors->first('username') }}</span>
          </div>
          <div class="form-group @if($errors->has('name')) @endif">
            <label for="name" class="bmd-label-floating">Name</label>
            <input type="text" class="form-control" id="name" name="name">
            <span class="text-danger">{{ $errors->first('name') }}</span>
          </div>
          <div class="form-group @if($errors->has('email')) @endif">
            <label for="email" class="bmd-label-floating">Email</label>
            <input type="text" class="form-control" id="email" name="email">
            <span class="text-danger">{{ $errors->first('email') }}</span>
          </div>
          <div class="form-group">
            <label for="alamat" class="bmd-label-floating @if($errors->has('alamat')) @endif">Alamat</label>
            <textarea name="alamat" class="form-control"></textarea>
            <span class="text-danger">{{ $errors->first('alamat') }}</span>
          </div>
          <div class="form-group @if($errors->has('password')) @endif">
            <label for="password" class="bmd-label-floating">Password</label>
            <input type="password" class="form-control" id="password" name="password">
            <span class="text-danger">{{ $errors->first('password') }}</span>
          </div>
          <div class="form-group @if($errors->has('re_password')) @endif">
            <label for="re_password" class="bmd-label-floating">Ulangi Password</label>
            <input type="password" class="form-control" id="re_password" name="re_password">
            <span class="text-danger">{{ $errors->first('re_password') }}</span>
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-primary">Daftar</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

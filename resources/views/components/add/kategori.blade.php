@extends('perpus')

@section('content')
<div class="row">
  <div class="col-4 offset-4">
    <div class="card">
      <div class="card-body">
        <form method="POST" action="{{ action('KategoriAdminController@store') }}">
          <div class="form-group @if($errors->has('kategori')) has-error @endif">
            <label for="kategori" class="bmd-label-floating">Kategori</label>
            <input type="text" class="form-control" id="kategori" name="kategori">
            <span class="text-danger">{{ $errors->first('kategori') }}</span>
          </div>
          <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
          <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

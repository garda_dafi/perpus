@extends('perpus')

@section('content')
<div class="row">
  <div class="col-4 offset-4">
    <div class="card">
      <div class="card-body">
        <form method="POST" action="{{ action('TagController@store') }}">
          <div class="form-group">
            <label for="tag" class="bmd-label-floating">Tag</label>
            <input type="text" class="form-control" id="tag" name="tag">
          </div>
          <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
          <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

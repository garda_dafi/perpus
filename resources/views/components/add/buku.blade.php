@extends('perpus')

@section('content')
<div class="row">
  <div class="col-4 offset-4">
    <div class="card">
      <div class="card-body">
        @if($errors->any())
        <div class="alert alert-danger">
          <p>Data yang dimasukan salah</p>
        </div>
        @endif
        <form method="POST" action="{{ action('BukuAdminController@store') }}" enctype="multipart/form-data" >
          <div class="form-group @if($errors->has('judul')) has-error @endif">
            <label for="judul" class="bmd-label-floating">Judul</label>
            <input type="text" class="form-control" id="judul" name="judul">
            <span class="text-danger">{{ $errors->first('judul') }} </span>
          </div>
          <div class="form-group @if($errors->has('penulis')) has-error @endif">
            <label for="penulis" class="bmd-label-floating">Penulis</label>
            <input type="text" class="form-control" id="penulis" name="penulis">
            <span class="text-danger">{{ $errors->first('penulis') }} </span>
          </div>
          <div class="form-group @if($errors->has('isbn')) has-error @endif">
            <label for="isbn" class="bmd-label-floating">ISBN</label>
            <input type="text" class="form-control" id="isbn" name="isbn">
            <span class="text-danger">{{ $errors->first('isbn') }} </span>
          </div>
          <div class="form-group @if($errors->has('sinopsis')) has-error @endif">
            <label for="sinopsis">Sinopsis</label>
            <textarea class="form-control" id="sinopsis" rows="3" name="sinopsis"></textarea>
            <span class="text-danger">{{ $errors->first('sinopsis') }} </span>
          </div>
          <div class="form-group">
            <label for="category">Kategori</label>
            <select class="form-control" id="category" name="kategori_id">
              @foreach($categories as $category)
                <option value="{{ $category->id }}">
                  {{ $category->kategori }}
                </option>
              @endforeach
            </select>
          </div>
          <div class="form-group @if($errors->has('stock')) has-error @endif">
            <label for="stock" class="bmd-label-floating">Stock</label>
            <input type="text" class="form-control" id="stock" name="stock">
            <span class="text-danger">{{ $errors->first('stock') }} </span>
          </div>
         <div class="form-group">
            <label for="cover">Cover</label>
            <input data-preview="#preview" name="cover" type="file" id="cover">
            <img class="col-sm-6" id="preview"  src="" ></img>
        </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

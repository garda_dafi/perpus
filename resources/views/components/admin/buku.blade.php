@extends('perpus')
@section('content')
  <div class="row">
    <div class="col-12">
      @if(session()->has('success-messages'))
      <div class="alert alert-success">
        {{ session()->get('success-messages') }}
      </div>
      @endif
      <a href="{{ action('BukuAdminController@create') }}" class="btn btn-primary">Tambah</a>
      <div class="table-responsive">
        <table class="table table-shopping">
            <thead>
                <tr>
                    <th class="text-center"></th>
                    <th>Buku</th>
                    <th>Kategori</th>
                    <th class="th-description">Sinopsis</th>
                    <th class="text-right">Stock</th>
                    <th class="text-right">Action</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
              @foreach ($books as $book)
                <tr>
                  <td>
                    <div class="img-container">
                      <img src="{{ asset('images/'.$book->url_cover) }}">
                    </div>
                  </td>
                  <td class="td-name">
                    <a href="#jacket">{{ $book->judul }}</a>
                    <br><small>by {{ $book->penulis }}</small>
                    <br><small>ISBN {{ $book->isbn }}</small>
                  </td>
                  <td style="text-transform:uppercase">{{ $book->kategori->kategori }}</td>
                  <td>
                    {{ $book->sinopsis }}
                  </td>
                  <td class="td-number">
                    {{ $book->stock-$book->peminjaman_count }}
                  </td>
                  <td class="td-actions">
                    <a href="{{ action('BukuAdminController@edit', $book->id) }}" data-placement="left" title="Remove item" class="btn btn-success">
                      <i class="material-icons">edit</i>
                    </a>
                    <a href="{{ action('BukuAdminController@delete', $book->id) }}" data-placement="left" title="Remove item" class="btn btn-danger">
                      <i class="material-icons">close</i>
                    </a>
                  </td>
                </tr>
              @endforeach
            </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

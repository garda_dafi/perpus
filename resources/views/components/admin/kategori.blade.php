@extends('perpus')
@section('content')
  <div class="row">
    <div class="col-6 offset-3">
      @if(session()->has('success-messages'))
      <div class="alert alert-success">
        {{ session()->get('success-messages') }}
      </div>
      @endif
      <a href="{{ action('KategoriAdminController@create') }}" class="btn btn-primary">Tambah</a>
      <table class="table" style="text-transform:uppercase">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>Kategori</th>
                <th class="text-right">Actions</th>
            </tr>
        </thead>
        <tbody>
          @foreach ($categories as $key=>$category)
            <tr>
              <td class="text-center">{{ ++$key }}</td>
              <td>{{ $category->kategori }}</td>
              <td class="td-actions text-right">
                <a href="{{ action('KategoriAdminController@edit',$category->id) }}" class="btn btn-success">
                  <i class="material-icons">edit</i>
                </a>
                <a href="{{ action('KategoriAdminController@delete', $category->id) }}" class="btn btn-danger">
                  <i class="material-icons">close</i>
                </a>
              </td>
            </tr>
          @endforeach
        </tbody>
    </table>
    </div>
  </div>

@endsection

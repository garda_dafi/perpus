@extends('perpus')
@section('content')
  <div class="row">
    <div class="col-4 offset-4">
      <div class="card card-profile">
        <div class="card-body">
          @if(session()->has('success-messages'))
          <div class="alert alert-success">
            {{ session()->get('success-messages') }}
          </div>
          @endif
          <h6 class="card-category text-gray">{{ Auth::user()->username }}</h6>
          <h4 class="card-title">{{ Auth::user()->name }}</h4>
          <p class="card-description">{{ Auth::user()->alamat }}</p>
          <a href="{{ action('UserController@edit', Auth::user()->id) }}" class="btn btn-primary btn-round">Ubah</a>
        </div>
      </div>
    </div>
  </div>
@endsection

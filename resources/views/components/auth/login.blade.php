@extends('perpus')

@section('content')
<div class="row">
  <div class="col-4 offset-4">
    <div class="card">
      <div class="card-body">
        @if(session()->has('error-messages'))
        <div class="alert alert-danger">
          {{ session()->get('error-messages') }}
        </div>
        @endif
        <form method="POST" action="{{ action('UserController@do_login') }}">
          <div class="form-group">
            <label for="username" class="bmd-label-floating">User Name</label>
            <input type="text" class="form-control" id="username" name="username">
          </div>
          <div class="form-group">
            <label for="password" class="bmd-label-floating">Password</label>
            <input type="password" class="form-control" id="password" name="password">
          </div>
          <div class="form-group">
            <a href="{{ route('password.request') }}">Lupa Password
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-primary">masuk</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

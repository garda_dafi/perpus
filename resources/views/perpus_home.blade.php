@extends('perpus')
@section('content')
  @foreach($categories as $category)
    @if(count($category->buku))
    <div class="alert alert-common">
      <h5 style="text-transform:uppercase; font-weight:bold">{{ $category->kategori }}</h5>
    </div>
    <div class="row">
      @foreach($category->buku as $book)
        <div class="col-3">
          <div class="card card-hover" style="width: 250px; margin-left:10px;margin-right:0; height:400px; position:relative">
            <img class="card-img-top" src="{{ asset('/images/'.$book->url_cover) }}" alt="Card image cap">
            <div class="card-body" style="position: absolute;top:210px; opacity:0; background-color:rgba(255,255,255,0.7);width:100%;height: 50%;bottom:0;color:#000">
              <h6 style="margin:0" class="card-title"><strong>{{ str_limit($book->judul, 40) }}</strong></h6>
              <p style="margin:0">Tersedia: {{ ($book->stock - $book->peminjaman_count) }}</p>
              <p class="card-text" style="font-size:0.8em">{{ str_limit($book->sinopsis, 100) }}</p>
            </div>
            <div style="width:100%;text-align:center;position:absolute;bottom:0;left:0">
              @if(($book->stock - $book->peminjaman_count) > 0)
                <a href="{{ action('BukuController@show', $book->id) }}" class="btn btn-primary">Lihat</a>
              @else
                <button class="btn btn-disabled">Habis</button>
              @endif
            </div>
          </div>
        </div>
      @endforeach
    </div>
  @endif
  @endforeach
@endsection

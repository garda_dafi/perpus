<!doctype html>
<html lang="en">
  <head>
    <title>Perpustakaan</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Material Dashboard CSS -->
    <link rel="stylesheet" href="{{ asset('css/material-dashboard.min.css') }}" type="text/css">

  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <nav class="navbar navbar-expand-lg bg-rose">
            <div class="container-fluid">
                <div class="navbar-translate">
                    <a class="navbar-brand" href="/">Perpustakaan</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        <span class="navbar-toggler-icon"></span>
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a href="{{ route('home') }}" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item" style="text-transform:uppercase">
                          <div class="dropdown show">
                            <a class="nav-link dropdown-toggle" href="#" role="button" id="dropDownKategori" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Kategori
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropDownKategori">
                              @foreach($categories as $category)
                                <a class="dropdown-item" href="{{ action('KategoriController@show', $category->id) }}">{{ $category->kategori }}</a>
                              @endforeach
                            </div>
                          </div>
                        </li>
                        <li class="nav-item">
                          @if(Auth::user())
                            <div class="dropdown show">
                              <a class="nav-link dropdown-toggle" href="#" role="button" id="dropDownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                              </a>
                              <div class="dropdown-menu" aria-labelledby="dropDownUser">
                                <a class="dropdown-item" href="{{ action('UserController@profile') }}">Profile</a>
                                <a class="dropdown-item" href="{{ action('PeminjamanController@index') }}">Buku Dipinjam</a>
                                <a class="dropdown-item" href="{{ action('UserController@do_logout') }}">Logout</a>
                              </div>
                            </div>
                          @else
                            <div class="dropdown show">
                              <a class="nav-link dropdown-toggle" href="#" role="button" id="dropDownAuth" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Masuk
                              </a>
                              <div class="dropdown-menu" aria-labelledby="dropDownAuth">
                                <a class="dropdown-item" href="{{ action('UserController@show_login') }}">Login</a>
                                <a class="dropdown-item" href="{{ action('UserController@create') }}">Register</a>
                              </div>
                            </div>
                          @endif
                        </li>
                        @if(Auth::user() && (Auth::user()->type == 0))
                        <li class="nav-item">
                          <div class="dropdown show">
                            <a class="nav-link dropdown-toggle" href="#" role="button" id="dropDownAdmin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              ADMIN
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropDownAdmin">
                              <a class="dropdown-item" href="{{ action('BukuAdminController@index') }}">Buku</a>
                              <a class="dropdown-item" href="{{ action('KategoriAdminController@index') }}">Kategori</a>
                              <a class="dropdown-item" href="{{ action('PeminjamanAdminController@index') }}">Peminjaman</a>
                            </div>
                          </div>
                        </li>
                        @endif
                    </ul>

                    <form class="form-inline ml-auto" method="GET" action="{{ action('BukuController@index') }}">
                        <div class="form-group has-white">
                          <input type="text" class="form-control" name="search" placeholder="Cari Buku">
                        </div>
                        <button type="submit" class="btn btn-white btn-raised btn-fab btn-fab-mini btn-round">
                            <i class="material-icons">search</i>
                        </button>
                    </form>
                </div>
            </div>
        </nav>
        </div> <!-- end column -->
      </div> <!-- end row -->
      @yield('content')
    </div> <!-- end container fluid -->

    <!--   Core JS Files   -->
    <script src="{{ asset('js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('js/core/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-design.js') }}"></script>

    <!-- Material Dashboard Core initialisations of plugins and Bootstrap Material Design Library -->
    <script src="{{ asset('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('js/material-dashboard.js?v=2.0.0') }}"></script>

    <script>
    $('document').ready(function(){
      $('.card-hover').hover(function(){
        $(this).find('.card-body').animate({
          'opacity': 1,
          'top': 200
        },200);
      },function(){
        $(this).find('.card-body').animate({
          'opacity': 0,
          'top': 210
        },200);
      });
    });
    </script>
  </body>
</html>

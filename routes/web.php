<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Buku;
use App\Kategori;

// Route::filter('admin', function(){
// });

Route::get('/', function () {
  $categories = Kategori::with(['buku'=> function($q){
    $q->withCount('peminjaman');
  }])->get();
  return view('perpus_home', [
    'categories' => $categories
  ]);
})->name('home');

Auth::routes();
Route::get('/password/reset', 'UserController@show_reset_password')->name('password.request');

Route::get('/login', 'UserController@show_login')->name('login');
Route::post('/login', 'UserController@do_login');
Route::get('/logout', 'UserController@do_logout');
// Route::get('/home', 'HomeController@index')->name('home');


Route::get('/user/profile', 'UserController@profile');
Route::post('/user/{id}', 'UserController@update');

Route::group(['middleware'=>['admin']], function(){
  Route::resource('/admin/buku','BukuAdminController');
  Route::get('/admin/buku/{id}/delete','BukuAdminController@delete');
  Route::post('/admin/buku/{id}/edit', 'BukuAdminController@update');
  Route::resource('/admin/kategori','KategoriAdminController');
  Route::post('/admin/kategori/{id}/edit', 'KategoriAdminController@update');
  Route::get('/admin/kategori/{id}/delete','KategoriAdminController@delete');
  Route::resource('/admin/peminjaman','PeminjamanAdminController');
  Route::get('/admin/peminjaman/{id}/delete','PeminjamanAdminController@delete');
});


Route::resource('/buku', 'BukuController');
Route::resource('/kategori', 'KategoriController');
Route::resource('/tag', 'TagController');
Route::resource('/user', 'UserController');
Route::resource('/peminjaman', 'PeminjamanController');
Route::get('/peminjaman/{id}/create', 'PeminjamanController@create');
Route::get('/peminjaman/{id}/delete', 'PeminjamanController@delete');

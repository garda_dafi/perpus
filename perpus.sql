-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 08, 2018 at 01:23 AM
-- Server version: 5.7.22-0ubuntu18.04.1
-- PHP Version: 7.2.3-1ubuntu1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpus`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `isbn` varchar(255) NOT NULL,
  `sinopsis` text NOT NULL,
  `url_cover` varchar(255) DEFAULT NULL,
  `kategori_id` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`) VALUES
(14, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('alif9f16@gmail.com', '$2y$10$fFelypylHTe2Tj/EVZfAGOsbpxVGykYYJAmqTFIMnKJvAs5/4iQby', '2018-04-30 21:22:58'),
('gardastronot.game@gmail.com', '$2y$10$7c2257vGKMCVsA17tHWvRuMmE81qSofrd.k84z55BdJQcESley4WW', '2018-05-07 11:21:00');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `buku_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(3) NOT NULL DEFAULT '2',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `alamat`, `url_avatar`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', 'fgdasfdas', 'fjkdasl@jfkasl', '$2y$10$t8Jz7aKTymvSSM5Fs7hLLurzYGKq839/Ia4wJL5Ryy0dmWceQJMlq', '', '', 0, NULL, '2018-04-30 02:42:43', '2018-04-30 02:42:43'),
(2, 'Garda Dafi', 'garda', 'garda.dafi@gmail.com', '$2y$10$6E8jUclG.sK.g3pl2es2nu3jLMM2VB4xOLx2RSMC6F1hZyOV1XEF.', 'Jl Moch toha, gang mula 1 no 13', '', 0, 'uRnBQByDjB2dFLqOpMawKlht78R4CH9IrfPxeKFtcWBXLn67Lo2XUWp2d1YN', '2018-04-30 03:01:25', '2018-05-03 07:58:07'),
(3, 'Super Admin', 'super_user', 'super_user@perpus.com', '$2y$10$3RgDT3Eeagn7MCmL/tS4KeWqDO3NZN0X003P/rhWtoRms2a1Rvfwu', 'Jl. Telekomunikasi No. 01, Terusan Buah Batu, Sukapura, Dayeuhkolot, Sukapura, Dayeuhkolot, Bandung, Jawa Barat 40257', '', 0, '7WMnRDk70zO7pFJmGoIJxFRKr1S2V96vSHRMQ8MnQHeXcWo6MGjhjQGD2H1Y', NULL, '2018-05-01 02:11:48'),
(5, NULL, 'garda_games', 'gardastronot.game@gmail.com', '$2y$10$FwmpbPUg5jPgllqfnklBEOK1wRuuFCOdNznpqSuvLcddg0ZmYrFZ.', NULL, NULL, 2, 'RVrsxnvP9qcEM28n4kWD1CVMiGwb01OXGj4GbtbeGT8Zf4nHMS3nPhRLa6uP', '2018-04-30 20:54:35', '2018-04-30 21:15:16'),
(6, NULL, 'alif', 'alif9f16@gmail.com', '$2y$10$JoiyZOVq8MM9.0KY5bZafOn.aNFFbCLHMnoX5ypeWmlVAmdNyTm/W', NULL, NULL, 2, NULL, '2018-04-30 21:22:49', '2018-04-30 21:22:49'),
(7, NULL, 'gardahuhuy', 'garda@game', '$2y$10$2YtKtGaGrz7LDuMpySnPN.fFuJL2.oox0o4t1mS8SOr17mCJ/mqu.', NULL, NULL, 2, NULL, '2018-04-30 21:28:00', '2018-04-30 21:28:00'),
(8, 'Garda Khadafi', 'garda123', 'garda.dafi@gmail.com', '$2y$10$bmkmaVuvqCji0EYUZKptX.E9c0sQZrPQ9ALcwywyfh9xfRy5kVnJq', 'Jl Moch Toha, gang mulya 1 no 13,ba\r\nJl Moch Toha', NULL, 2, NULL, '2018-05-01 02:18:25', '2018-05-01 02:18:25'),
(9, 'test', 'test', 'test@test', '$2y$10$sIui78eA0pqBgkQNjSSqoeDpKZFv66l/GkooGD5r9/n8k7SC6cfiK', 'fjdkasl', NULL, 1, '8xGlCe3bWv6tNHQqPUPbxfypgouguW3MfeULWFZtx7FALzpavkeYDXoQJ2PG', '2018-05-01 02:51:20', '2018-05-01 02:51:20'),
(10, 'Azka khoi', 'azka', 'azka@azka', '$2y$10$EDIIyawHni6LpHxeai.LR.h1nQ9yuelJ9r4qLOdQqrmIu6H/7HRu2', NULL, NULL, 1, 'kDI122ouq2qmtACQNwJp35pubvvnPyYhfdYNIc2noJPLectDJKVETKusdydL', '2018-05-03 07:53:43', '2018-05-03 07:53:43'),
(11, 'Rizki Pujianto', 'rizki', 'rizki@123', '$2y$10$nAszphSxBKADdS0BkGgT5uTfrjJkDQuG/lBZuyZYbfUmz2IzK9nGi', NULL, NULL, 1, 'aFLisONiD983NVJJxapHnuj4oTPgzEt6gzCwOepj9888XtoxwKt1fQkA2Iq9', '2018-05-03 07:54:17', '2018-05-03 07:54:17'),
(13, 'Alif Yonanta', 'alif123', 'alif@123', '$2y$10$SqN7/5VQnmw0FGYwIG5LA.e6FHbnorpUFMKAuYwCorXUXsdmpAfny', NULL, NULL, 1, 'MWUnk5IkBBhlbvgUZKk2nUWwEk1MRTTj3qNn8feDO648p9ZWU2qILUsIXMO5', '2018-05-03 07:55:04', '2018-05-03 07:55:04'),
(14, 'asdfas', 'asdfa', 'asdfa', '$2y$10$eKqmW.m966vpqcAc00smcOwS0kwGUD0/LFiCBf3H9bYSkNUeBV8g6', 'asdfa', NULL, 1, NULL, '2018-05-07 11:19:45', '2018-05-07 11:19:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Buku;
use App\Peminjaman;
use Validator;

class KategoriAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $kategori = Kategori::all();
      return view('components.admin.kategori', [
        'categories' => $kategori
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $categories = Kategori::all();
      return view('components.add.kategori', [
        'categories' => $categories
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $rules = Kategori::rules();
      $validator = Validator::make($request->all(), $rules);
      if($validator->fails()){
        return redirect()->back()->withErrors($validator);
      }else{
        $kategori = new Kategori;
        $kategori->kategori = $request->kategori;

        if($kategori->save()){
          return redirect()->action('KategoriAdminController@index')->with('success-messages', 'Kategori berhasil ditambahkan');
        }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $kategori = Kategori::find($id);
      $categories = Kategori::all();
      return view('components.edit.kategori', [
        'kategori' => $kategori,
        'categories' => $categories
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = Kategori::rules_update();
      $validator = Validator::make($request->all(), $rules);
      if($validator->fails()){
        return redirect()->back()->withErrors($validator);
      }else{
        $kategori = Kategori::find($id);
        $kategori->kategori = $request->kategori;

        if($kategori->save()){
          return redirect()->action('KategoriAdminController@index')->with('success-messages', 'Kategori berhasil diubah');
        }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
    public function delete($id){
      Kategori::destroy($id);
      Peminjaman::whereHas('buku',function($q)use($id){
        $q->where('kategori_id', $id);
      })->delete();
      Buku::where('kategori_id', $id)->delete();
      return redirect()->action('KategoriAdminController@index')->with('success-messages', 'Kategori berhasil dihapus');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Kategori;
use Validator;
use Hash;
use Auth;
use Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $categories = Kategori::all();
      return view('components.add.user', [
        'categories' => $categories
      ]);
    }
    public function show_reset_password(){
      $categories = Kategori::all();
      return view('auth.passwords.email', [
        'categories' => $categories
      ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $rules = User::rules();
      $validator = Validator::make($request->all(), $rules);
      if($validator->fails()){
        return redirect()->back()->withErrors($validator);
      }else{
        $user = new User;
        $user->username = $request->username;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->alamat = $request->alamat;
        $user->type = 1;
        $user->password = Hash::make($request->password);
        if($user->save()){
          return redirect()->route('login');
        }
        return 'user gagal ditambahkan';
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function show_login(){
      $categories = Kategori::all();
      return view('components.auth.login', [
        'categories' => $categories
      ]);
    }

    public function do_login(Request $request){
      $rules = User::rules_login();
      $validator = Validator::make($request->all(), $rules);
      if($validator->fails()){
        return $validator->errors();
      }else{
        $userdata = [
          'username' => $request->username,
          'password' => $request->password
        ];
        if(Auth::attempt($userdata)){
          return redirect()->route('home');
        }
        return redirect()->back()->with('error-messages', 'Username atau password salah');
      }
    }


    public function do_logout(){
      Auth::logout();
      return Redirect::to('login');
    }

    public function profile(){
      $categories = Kategori::all();
      return view('components.auth.profile',[
        'categories' => $categories
      ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $categories = Kategori::all();
      return view('components.edit.user',[
        'categories' => $categories
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = User::rules_update();
      $validator = Validator::make($request->all(), $rules);
      if($validator->fails()){
        return $validator->errors();
      }else{
        $user = User::find($id);
        $user->username = $request->username;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->alamat = $request->alamat;
        if($request->password){
          $user->password = Hash::make($request->password);
        }
        if($user->save()){
          return redirect()->action('UserController@profile')->with('success-messages', 'Data berhasil diubah');
        }
        return redirect()->back();
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

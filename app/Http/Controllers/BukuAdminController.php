<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\Kategori;
use Validator;

class BukuAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $books = Buku::with('kategori')->withCount('peminjaman')->get();
      $categories = Kategori::all();
      return view('components.admin.buku', [
        'books' => $books,
        'categories' => $categories
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Kategori::all();
        return view('components.add.buku', [
          'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $rules = Buku::rules();
      $validator = Validator::make($request->all(), $rules);
      if($validator->fails()){
        return redirect()->back()->withErrors($validator)->withInput($request->input());
      }else{
        $buku = new Buku;
        $buku->judul = $request->judul;
        $buku->penulis = $request->penulis;
        $buku->isbn = $request->isbn;
        $buku->sinopsis = $request->sinopsis;
        $buku->kategori_id = $request->kategori_id;
        $buku->stock = $request->stock;
        if($request->cover){
          $imageName = time().'.'.$request->cover->getClientOriginalExtension();
          $buku->url_cover = $imageName;
          $request->cover->move(public_path('images'), $imageName);
        }
        if($buku->save()){
          return redirect()->action('BukuAdminController@index')->with('success-messages', 'Buku berhasil ditambahkan');
        }
        return redirect()->back();
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $categories = Kategori::all();
      $buku = Buku::find($id);
      return view('components.edit.buku',[
        'categories' => $categories,
        'buku' => $buku
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = Buku::rules_update();
      $validator = Validator::make($request->all(), $rules);
      if($validator->fails()){
        return redirect()->back()->withErrors($validator)->withInput($request->input());
      }else{
        $buku = Buku::find($id);
        $buku->judul = $request->judul;
        $buku->penulis = $request->penulis;
        $buku->isbn = $request->isbn;
        $buku->sinopsis = $request->sinopsis;
        $buku->kategori_id = $request->kategori_id;
        $buku->stock = $request->stock;
        if($request->cover){
          $imageName = time().'.'.$request->cover->getClientOriginalExtension();
          $buku->url_cover = $imageName;
          $request->cover->move(public_path('images'), $imageName);
        }
        if($buku->save()){
          return redirect()->action('BukuAdminController@index')->with('success-messages', 'Buku berhasil diubah');
        }
        return redirect()->back();
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id){
      Buku::destroy($id);
      return redirect()->action('BukuAdminController@index')->with('success-messages', 'Buku berhasil dihapus');;
    }
}

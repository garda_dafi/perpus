<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
  protected $table = 'tag';
    public static function rules(){
      return [
        'id_buku' => 'required',
        'tag' => 'required'
      ];
    }

    public function buku(){
      return $this->belongsTo('App\Buku', 'id_buku');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
  protected $table = 'peminjaman';

  public function user(){
    return $this->belongsTo('App\User');
  }

  public function buku(){
    return $this->belongsTo('App\Buku');
  }
}

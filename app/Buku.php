<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
  protected $table = 'buku';
    public static function rules(){
      return [
        'judul' => 'required',
        'penulis' => 'required',
        'isbn' => 'required',
        'sinopsis' => 'required',
        'kategori_id' => 'required',
        'stock' => 'numeric'
      ];
    }
    public static function rules_update(){
      return [
        'judul' => 'required',
        'penulis' => 'required',
        'isbn' => 'required',
        'sinopsis' => 'required',
        'kategori_id' => 'required',
        'stock' => 'numeric'
      ];
    }

    public function kategori(){
      return $this->belongsTo('App\Kategori');
    }

    public function tag(){
      return $this->hasMany('App\Tag');
    }

    public function peminjaman(){
      return $this->hasMany('App\Peminjaman');
    }
}

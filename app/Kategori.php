<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
  protected $table = 'kategori';
  public $timestamps = false;

    public static function rules(){
      return [
        'kategori' => 'required'
      ];
    }

    public static function rules_update(){
      return [
        'kategori' => 'required'
      ];
    }

    public function buku(){
      return $this->hasMany('App\Buku');
    }
}
